# Node

This project requires NodeJS to run the main script. Recommend [nvm](https://github.com/nvm-sh/nvm) as a simple way to install and use Node.

# Twilio

This project uses Twilio to send text messages. You can create a free account at https://www.twilio.com/ to recieve an auth token, account id, and twilio number. Alternatively, you can remove the twilio code and choose another method of notifying yourself.

# .ENV file

You'll need to create a .ENV file in the root of this project to store some configuration. See the example file for guidance on the contents

# Getting Started

Install dependencies

`npm i`

Run the script

`node main.js`

