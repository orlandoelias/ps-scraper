// Read the .ENV config file
require("dotenv").config();

const axios = require("axios");
const cheerio = require("cheerio");
const twilio = require("twilio");

// Environment variables stored in .ENV file
const {
  ACCOUNT_SID,
  AUTH_TOKEN,
  TWILIO_NUMBER,
  MY_NUMBER
} = process.env;

const URL = "https://direct.playstation.com/en-us/ps5";

// Initialize twilio client
const client = new twilio(ACCOUNT_SID, AUTH_TOKEN);

/**
 * Utility function to log messages
 * 
 * @param {string} level 
 * @param {string} message 
 */
const log = (level, message) => {
  console.info(`[${level}]-[${new Date().toISOString()}] ==> ${message}`);
};

/**
 * Creates a twilio message and
 * sends it to the given number
 * 
 * @param {string} message 
 * @param {number} number 
 */
const sendMessage = (message, number) => {
  client.messages
    .create({
      body: message,
      to: number,
      from: TWILIO_NUMBER,
    })
    .then((message) => log("info", `Message sent successfull - ${message.sid}`))
    .catch((err) => log("error", err));
};

/**
 * Checks the stock of the PS5, sends a message if the placeholder message is missing
 * (meaning PS5 is in stock)
 */
const checkStock = () => {
  log("info", "Sending request");
  axios.get(URL).then((res) => {
    const $ = cheerio.load(res.data);
    const outOfStock = $(
      ".hero-detail-card .description-placeholder strong"
    ).first();

    if (!outOfStock) {
      sendMessage(`PLACEHOLDER NOT FOUND - CHECK STOCK AT ${URL}`, MY_NUMBER);
    } else {
      log("info", "Product still out of stock");
    }
  });
};

// Main function, checks stock every 5 seconds
const main = () => {
  const INTERVAL = 5 *1000; // 5 seconds

  setInterval(checkStock, INTERVAL);
};

main();
